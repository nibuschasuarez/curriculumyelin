import { Component, OnInit } from '@angular/core';
import { ReunionDataI } from 'src/app/Interfaces/Citas.interface';
import { MatTableDataSource } from '@angular/material/table';


@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.css']
})
export class AgendaComponent implements OnInit { 
  
  citaUsuarios: ReunionDataI [] = [];

  displayedColumns: string[] = ['nombre', 'apellido','email', 'telefono', 'descripcion', 'fecha', 'hora', 'modoR']; 
  dataSource!: MatTableDataSource<any>;

  constructor( ) { }

  ngOnInit(): void {
  }

}
